from sqlalchemy import Column, ForeignKey, Integer, String, UniqueConstraint
from sqlalchemy.orm import relationship

from .db import db
from .department import Department


class Location(db.Model):
    __tablename__ = 'locations'

    id = Column(Integer, primary_key=True)
    name = Column(String(128), nullable=False)
    description = Column(String(512), nullable=True)

    parent_location_id = Column(Integer, ForeignKey('locations.id', onupdate="CASCADE", ondelete="SET NULL"), nullable=True)
    parent_location = relationship('Location', foreign_keys=[parent_location_id])

    department_id = Column(Integer, ForeignKey('departments.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    department = relationship(Department, foreign_keys=[department_id])

    UniqueConstraint(department_id, name, name="uix__locations__departement_id__name")