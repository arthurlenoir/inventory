from datetime import datetime
from sqlalchemy import Column, DateTime, Integer, String

from .db import db

class User(db.Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(128), unique=True, nullable=False)
    password = Column(String(128), nullable=False)
    salt = Column(String(32), nullable=False)
    creation_date = Column(DateTime, nullable=False, default=datetime.utcnow)