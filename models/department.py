from datetime import datetime
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .db import db
from .user import User


class Department(db.Model):
    __tablename__ = 'departments'

    id = Column(Integer, primary_key=True)
    name = Column(String(128), unique=True, nullable=False)
    description = Column(String(512), nullable=True)
    display_by = Column(String(32), default="location")
    sort_by = Column(String(32), default="name")


class UserDepartmentAccess(db.Model):
    __tablename__ = 'users_departements_access'

    id = Column(Integer, primary_key=True)

    user_id = Column(Integer, ForeignKey('users.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    user = relationship(User, foreign_keys=[user_id])

    department_id = Column(Integer, ForeignKey('departments.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    department = relationship(Department, foreign_keys=[department_id])