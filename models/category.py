from sqlalchemy import Column, ForeignKey, Integer, String, UniqueConstraint
from sqlalchemy.orm import relationship

from .db import db
from .department import Department


class Category(db.Model):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    name = Column(String(128), nullable=False)
    description = Column(String(512), nullable=True)

    department_id = Column(Integer, ForeignKey('departments.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    department = relationship(Department, foreign_keys=[department_id])

    UniqueConstraint(department_id, name, name="uix__categories__departement_id__name")