from datetime import datetime
from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String, UniqueConstraint
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship

from .db import db
from .category import Category
from .department import Department
from .location import Location


class Item(db.Model):
    __tablename__ = 'items'

    id = Column(Integer, primary_key=True)
    name = Column(String(128), nullable=False)
    description = Column(String(512), nullable=True)
    addtional_data = Column(JSONB, nullable=True)
    quantity = Column(Integer)
    quantity_per_unit = Column(String(128), nullable=True)
    active = Column(Boolean, default=True, nullable=False)

    creation_date = Column(DateTime, nullable=False, default=datetime.utcnow)
    last_update_date = Column(DateTime, nullable=False, default=datetime.utcnow)

    location_id = Column(Integer, ForeignKey('locations.id', onupdate="CASCADE", ondelete="SET NULL"), nullable=True)
    location = relationship('Location', foreign_keys=[location_id])

    department_id = Column(Integer, ForeignKey('departments.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    departement = relationship(Department, foreign_keys=[department_id])

    category_id = Column(Integer, ForeignKey('categories.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=True)
    category = relationship(Category, foreign_keys=[category_id])

    UniqueConstraint(department_id, name, name="uix__items__departement_id__name")