import os

PROD = os.environ.get('PROD', False)
DEBUG = os.environ.get('DEBUG', not PROD)

DB_PROTOCOL = os.environ.get('DB_PROTOCOL', 'postgresql+psycopg2')
DB_HOST = os.environ.get('DB_HOST', 'localhost')
DB_USER = os.environ.get('DB_USER', '')
DB_PWD = os.environ.get('DB_PWD', '')
DB_PORT = int(os.environ.get('DB_PORT', 5432))
DB_DATABASE = os.environ.get('DATABASE', 'inventory')

DATABASE_URI = "%s://%s:%s@%s:%d/%s" % (
    DB_PROTOCOL,
    DB_USER,
    DB_PWD,
    DB_HOST,
    DB_PORT,
    DB_DATABASE
)
